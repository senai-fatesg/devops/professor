package com.franciscocalaca.cadastro;

import org.junit.Test;

public class ExemploComplexidadeCiclomaticaTest {

	private ExemploComplexidadeCiclomatica ecc = new ExemploComplexidadeCiclomatica();
	
	@Test
	public void teste1() {
		ecc.teste(11);
	}

	@Test
	public void teste2() {
		ecc.teste(5);
	}

}
