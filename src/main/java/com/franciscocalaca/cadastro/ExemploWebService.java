package com.franciscocalaca.cadastro;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExemploWebService {

	@GetMapping("/ok")
	public String getOk() {
		return "OK! Funciona mesmo!! :)";
	}
	
}
